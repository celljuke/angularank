'use strict';

describe('Component: mainView', function () {

  // load the controller's module
  beforeEach(module('angularankApp'));

  var $componentController, $ctrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function(_$componentController_) {
    $componentController = _$componentController_;

    $ctrl = $componentController('mainView', null, null);
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect($ctrl.viewTitle).toBe('Contributors');
  });
});
