'use strict';

angular.module('angularankApp')
  .component('repoDetailView', {
    templateUrl: 'components/repo-detail-view/repo-detail-view.component.html',
    require: {
      mainViewCtrl: '^mainView'
    },
    bindings: {
      repo: '<',
      contributorsOfRepo: '<'
    },
    controller: function () {
      var $ctrl = this;
      $ctrl.activeTab = 1;
      
      this.setActiveTab = function (tab) {
        $ctrl.activeTab = tab;
      }

      this.isActiveTab = function (tab) {
        return $ctrl.activeTab === tab;
      };
      
      this.onUserCardClick = function (user) {
        this.mainViewCtrl.onUserCardClick(user);
      }
    }
  });