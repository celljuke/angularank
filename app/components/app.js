'use strict';

angular
  .module('angularankApp', ['ngAnimate', 'angular-click-outside', 'rzModule'])
  .component('app', {
    templateUrl: 'components/app.html',
    bindings: {
      userName: '@'
    },
    controller: function ($q, $http, GitHubData) {
      var $ctrl = this;
      $ctrl.contrData = [];
      $ctrl.filteredContrData = [];

      this.setUser = function (user) {
        $ctrl.userName = user;
      }

      this.setUser("angular");

      this.setUserDetail = function (user) {

        $ctrl.activeUser = null;
        $ctrl.activeRepo = null;
        $ctrl.isDataLoaded = false;

        OAuth.initialize('pK2KmxfzbUSIaFyxOFIkQLUrJiE');
        OAuth.popup('github')
          .done(function (result) {
            GitHubData.getUserDetail(user.login, result).then(function (response) {
              var repoCount = response.public_repos;

              // do not show huge count of repos b/c of performance problems
              repoCount = repoCount > 500 ? 500 : repoCount;

              var totalRepoPage = Math.ceil(repoCount / 100);
              var reposOfUser = [];
              var nestedPromises = _.times(totalRepoPage, function (index) {
                var deferred = $q.defer();
                return GitHubData.getReposOfUser(user.login, {page: index + 1, per_page: 100}, result)
                  .then(function (response) {
                    var data = response;
                    if (angular.isArray(data)) {
                      if (data.length > 0) {
                        deferred.resolve(data);
                        return deferred.promise;
                      }
                    }
                  })
              })

              var promises = Array.prototype.concat.apply([], nestedPromises);
              $q.all(promises).then(function (orderData) {
                _.times(orderData.length, function (index) {
                  reposOfUser = reposOfUser.concat(orderData[index]);
                });
                $ctrl.activeUser = response;
                $ctrl.activeUser.repos = _.orderBy(reposOfUser, 'stargazers_count', 'desc');
                $ctrl.isDataLoaded = true;
              });
            })
          });
      }

      this.setRepoDetail = function (repo) {
        $ctrl.isDataLoaded = false;
        $ctrl.prevUser = $ctrl.activeUser;

        var contrOfRepo = [];
        OAuth.initialize('pK2KmxfzbUSIaFyxOFIkQLUrJiE');
        OAuth.popup('github')
          .done(function (result) {
            GitHubData.getRepoDetail($ctrl.activeUser.login, repo.name).then(function (response) {

              var nestedPromises = _.times(5, function (pageIndex) {
                var deferred = $q.defer();
                return GitHubData.getContributorsByRepo($ctrl.activeUser.login, repo.name, pageIndex + 1, result)
                  .then(function (response) {
                    deferred.resolve(response);
                    return deferred.promise;
                  })
              });

              var promises = Array.prototype.concat.apply([], nestedPromises);
              $q.all(promises).then(function (orderData) {
                _.times(orderData.length, function (index) {
                  contrOfRepo = contrOfRepo.concat(orderData[index]);
                });

                $ctrl.activeRepo = response.data;
                $ctrl.activeRepo.contributors = _.orderBy(contrOfRepo, 'contributions', 'desc');
                $ctrl.isDataLoaded = true;
                $ctrl.activeUser = null;
              });
            })
          });
      }

      this.filterContributors = function (data) {
        $ctrl.filteredContrData = _.filter($ctrl.contrData,
          function (contr) {
            return contr.user_info
              && contr.user_info.followers <= data.followers.maxValue
              && contr.user_info.followers >= data.followers.minValue
              && contr.contributions <= data.commits.maxValue
              && contr.contributions >= data.commits.minValue
              && contr.user_info.public_repos <= data.repos.maxValue
              && contr.user_info.public_repos >= data.repos.minValue;
          });
      }

      $http({
        method: 'GET',
        url: 'assets/contributors.json'
      }).then(function (response) {

        $ctrl.isDataLoaded = true;

        $ctrl.contrData = _.orderBy(response.data, 'contributions', 'desc');
        $ctrl.filteredContrData = _.orderBy(response.data, 'contributions', 'desc');
        $ctrl.setUserDetail($ctrl.filteredContrData[0]);

      }, function (error) {

      });


      // You can use real api connection with github

      /*GitHubData.getAllContributorsOfUser($ctrl.userName)
       .then(function (response) {
        $ctrl.isDataLoaded = true;
        $ctrl.contrData = response;
        $ctrl.filteredContrData = response;
        $ctrl.setUserDetail(response[0]);
       });*/

    }
  });
