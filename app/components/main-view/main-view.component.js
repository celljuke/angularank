'use strict';

angular.module('angularankApp')
  .component('mainView', {
    templateUrl: 'components/main-view/main-view.component.html',
    transclude: true,
    require: {
      appCtrl: '^app'
    },
    bindings: {
      contributors: '<',
      activeUser: '<',
      activeRepo: '<',
      isDataLoaded: '<'
    },
    controller: function (orderByFilter) {

      var $ctrl = this;
      $ctrl.viewTitle = 'Contributors';
      $ctrl.isMenuVisible = false;

      $ctrl.sortType = 'contributions'
      $ctrl.sortReverse = true;

      this.setRepoDetail = function (repo) {

        $ctrl.appCtrl.setRepoDetail(repo);

      }
      
      this.toggleSearchMenu = function () {
        $ctrl.isMenuVisible = !$ctrl.isMenuVisible;
      }

      this.closeThis = function () {
        $ctrl.isMenuVisible = false;
      }
      
      this.onUserCardClick = function (user) {
        this.appCtrl.setUserDetail(user);
      }

    }
  });

