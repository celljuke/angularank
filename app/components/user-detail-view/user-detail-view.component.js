'use strict';

angular.module('angularankApp')
  .component('userDetailView', {
    templateUrl: 'components/user-detail-view/user-detail-view.component.html',
    bindings: {
      user: '<'
    },
    controller: function ($q, GitHubData) {
      var $ctrl = this;
      $ctrl.activeTab = 1;

      this.setActiveTab = function (tab) {
        $ctrl.activeTab = tab;
      }

      this.isActiveTab = function (tab) {
        return $ctrl.activeTab === tab;
      };

    }
  });