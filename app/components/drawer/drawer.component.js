'use strict';

angular.module('angularankApp')
  .component('drawer', {
    templateUrl: 'components/drawer/drawer.component.html',
    transclude: true,
    require: {
      appCtrl: '^app'
    },
    bindings: {
      userName: '@'
    },
    controller: function (GitHubData) {
      var $ctrl = this;

      OAuth.initialize('pK2KmxfzbUSIaFyxOFIkQLUrJiE');
      OAuth.popup('github')
        .done(function (result) {
          GitHubData.getUserDetail($ctrl.userName, result)
            .then(function (response) {
              $ctrl.userDetail = response;
            })
        });


      this.getFilter = function () {
        return {
          followers: {minValue: $ctrl.followerSlider.minValue, maxValue: $ctrl.followerSlider.maxValue},
          commits: {minValue: $ctrl.commitSlider.minValue, maxValue: $ctrl.commitSlider.maxValue},
          repos: {minValue: $ctrl.repoSlider.minValue, maxValue: $ctrl.repoSlider.maxValue}
        }
      }

      $ctrl.commitSlider = {
        minValue: 0,
        maxValue: 500,
        options: {
          floor: 0,
          ceil: 1000,
          onChange: function () {
            $ctrl.appCtrl.filterContributors($ctrl.getFilter());
          }
        }
      };

      $ctrl.followerSlider = {
        minValue: 0,
        maxValue: 5000,
        options: {
          floor: 0,
          ceil: 10000,
          onChange: function () {
            $ctrl.appCtrl.filterContributors($ctrl.getFilter());
          }
        }
      };

      $ctrl.repoSlider = {
        minValue: 0,
        maxValue: 250,
        options: {
          floor: 0,
          ceil: 500,
          onChange: function () {
            $ctrl.appCtrl.filterContributors($ctrl.getFilter());
          }
        }
      };

    }
  });