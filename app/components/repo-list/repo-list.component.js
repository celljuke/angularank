'use strict';

angular.module('angularankApp')
  .component('repoList', {
    templateUrl: 'components/repo-list/repo-list.component.html',
    require: {
      mainViewCtrl: '^mainView'
    },
    bindings: {
      repos: '<',
      maxRecord: '@',
      searchEnabled: '@'
    },
    controller: function () {
      var $ctrl = this;

      this.openRepo = function (repo) {
        $ctrl.mainViewCtrl.setRepoDetail(repo);
      }
    }
  });