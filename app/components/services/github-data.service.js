angular.module('angularankApp')
  .constant('GitHubDataConfig', {
    API_ENDPOINT: 'https://api.github.com/',
    PAGE_SIZE: 100
  })
  .factory('GitHubData', function ($http, $q, GitHubDataConfig) {
    'use strict';

    OAuth.initialize('pK2KmxfzbUSIaFyxOFIkQLUrJiE');

    return {
      getContributorsByRepo: getContributorsByRepo,
      getReposOfUser: getReposOfUser,
      getUserDetail: getUserDetail,
      getAllContributorsOfUser: getAllContributorsOfUser,
      getRepoDetail: getRepoDetail
    };

    function getContributorsByRepo(user, repo, pageIndex, dataSource) {
      var deferred = $q.defer();


      dataSource.get(GitHubDataConfig.API_ENDPOINT + 'repos/' + user + '/' + repo + '/contributors?page=' + pageIndex + '&per_page=' + GitHubDataConfig.PAGE_SIZE)
        .done(function (response) {
          deferred.resolve(response);
        })
        .fail(function (err) {
          return err;
        });


      return deferred.promise;
    }

    function getReposOfUser(user, data, dataSource) {
      var deferred = $q.defer();


      dataSource.get(GitHubDataConfig.API_ENDPOINT + 'users/' + user + '/repos?page=' + data.page + '&per_page=' + data.per_page)
        .done(function (response) {
          deferred.resolve(response);
        })
        .fail(function (err) {
          return err;
        });

      return deferred.promise;
    }

    function getUserDetail(user, dataSource) {
      var deferred = $q.defer();

      dataSource.get(GitHubDataConfig.API_ENDPOINT + 'users/' + user)
        .done(function (response) {
          deferred.resolve(response);
        })
        .fail(function (err) {
          return err;
        });

      return deferred.promise;
    }

    function getAllContributorsOfUser(user) {
      var deferred = $q.defer();
      OAuth.popup('github')
        .done(function (result) {
          var totalRepoNumber = 0,
            totalRepoPage = 0,
            reposOfUser = [],
            allContributors = [],
            prom = [];


          getUserDetail(user, result).then(function (response) {
            var data = response;
            if (data) {
              totalRepoNumber = data.public_repos;
              totalRepoPage = Math.ceil(totalRepoNumber / GitHubDataConfig.PAGE_SIZE);
            }

            var nestedPromises = _.times(totalRepoPage, function (index) {
              var deferred = $q.defer();
              return getReposOfUser(user, {page: index + 1, per_page: GitHubDataConfig.PAGE_SIZE}, result)
                .then(function (response) {
                  var data = response;
                  if (angular.isArray(data)) {
                    if (data.length > 0) {
                      deferred.resolve(data);
                      return deferred.promise;
                    }
                  }
                })
            });

            var promises = Array.prototype.concat.apply([], nestedPromises);
            $q.all(promises).then(function (orderData) {
              _.times(orderData.length, function (index) {
                reposOfUser = reposOfUser.concat(orderData[index]);
              });
              _.forEach(reposOfUser, function (repo, index) {
                _.times(5, function (pageIndex) {
                  getContributorsByRepo(user, repo.name, pageIndex + 1, result)
                    .then(function (response) {
                      if (response) {
                        _.forEach(response, function (contributor) {
                          if (!_.some(allContributors, ['login', contributor.login])) {
                            allContributors.push(contributor);
                          } else {
                            var index = _.indexOf(allContributors, _.find(allContributors, {id: contributor.id}));
                            var contributionAmount = allContributors[index].contributions + contributor.contributions;
                            _.set(allContributors, [index, 'contributions'], contributionAmount);
                          }
                        });

                        if (index + 1 === reposOfUser.length) {
                          _.forEach(allContributors, function (cont) {
                            var deferred = $q.defer();
                            prom.push(getUserDetail(cont.login, result)
                              .then(function (response) {
                                cont.user_info = response;
                                deferred.resolve(cont);
                                return deferred.promise;
                              }))
                          });

                          $q.all(prom).then(function (lastContrData) {
                            
                            lastContrData = _.orderBy(lastContrData, 'contributions', 'desc');
                            deferred.resolve(lastContrData);
                          });

                        }
                      }
                    });
                });
              });
            }).catch(function (err) {
              console.log(err);
            });
          });
        })
        .fail(function (err) {
          return err;
        });

      return deferred.promise;

    }

    function getRepoDetail(user, repo) {
      var deferred = $q.defer();

      $http({
        method: 'GET',
        url: GitHubDataConfig.API_ENDPOINT + 'repos/' + user + '/' + repo
      }).then(function (response) {

        deferred.resolve(response);

      }, function (error) {
        deferred.reject(error)
      });


      return deferred.promise;
    }

  });