# angularank

## Dependencies

* ruby >= 2.1.2
    * sass >= 3.4.18
    * compass >= 1.0.3
* npm >= 2.14.12
    * node >= 4.3.1

## Github API dependencies

* oauth.io
    * authentication required for github api:
        * user name = celljuke
        * password = scalac2016

## Installing

Run `npm install`

Run `bower install`

## Build

Run `grunt build` for building it creates `dist` folder.

## Development

Run `grunt serve` for preview. It will be work on http://localhost:9000

## Testing

Running `grunt test` will run the unit tests with karma.
